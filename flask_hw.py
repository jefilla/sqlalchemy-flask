import numpy as np
import pandas as pd
import sqlalchemy
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine, func

from flask import Flask, jsonify


#################################################
# Database Setup
#################################################
engine = sqlalchemy.create_engine("sqlite:///Resources/hawaii.sqlite")


#################################################
# Flask Setup
#################################################
app = Flask(__name__)


#################################################
# Flask Routes
#################################################

@app.route("/")
def welcome():
    """List all available api routes."""
    return (
        f"Available Routes:<br/>"
        f"/api/v1.0/precipitation<br/>"
        f"/api/v1.0/stations<br/>"
        f"/api/v1.0/tobs<br/>"
        f"/api/v1.0/2011-01-01<br/>"
        f"/api/v1.0/2013-12-01/2015-01-01<br/>"
    )




@app.route("/api/v1.0/precipitation")
def precipitation():
    
    """ retrieve the last 12 months of precipitation data. """
    max_date = ''
    max_date_sql = """SELECT max(date) FROM measurement"""
 
    connection = engine.connect()
    result = connection.execute(max_date_sql)

    for row in result:
        max_date = row['max(date)']
        print(max_date)

    last_year_prcp_sql = "SELECT prcp from measurement where date > date('"+max_date+"', '-365 days')"
    result = connection.execute(last_year_prcp_sql)

    all_precip = []

    for row in result:
        prcp_dict = {}
        prcp_dict['prcp'] = row['prcp']
        all_precip.append(prcp_dict)


    connection.close()
    return jsonify(all_precip)


@app.route("/api/v1.0/stations")
def station():
    
    """ Return a list of all stations """
    station_list_sql = "SELECT station FROM station"
    connection = engine.connect()
    result = connection.execute(station_list_sql)

    all_stations = []
    for row in result:
        station_dict = {}
        station_dict["name"] = row['station']
        all_stations.append(station_dict)

    connection.close()
    return jsonify(all_stations)
       

@app.route("/api/v1.0/tobs")
def temperature_observations():  
    """ the dates and temperature observations from a year from the last data point """

    max_date = ''
    max_date_sql = """SELECT max(date) FROM measurement"""
 
    connection = engine.connect()
    result = connection.execute(max_date_sql)

    for row in result:
        max_date = row['max(date)']
        print(max_date)    

    last_year_prcp_date_sql = "SELECT date, prcp from measurement where date > date('"+max_date+"', '-365 days')"
    result = connection.execute(last_year_prcp_date_sql)

    all_date_precip = []

    for row in result:
        all_date_prcp_dict = {}
        all_date_prcp_dict['date'] = row['date']
        all_date_prcp_dict['prcp'] = row['prcp']
        all_date_precip.append(all_date_prcp_dict)

    connection.close()
    return jsonify(all_date_precip)


@app.route("/api/v1.0/<start>/<end>")
def calc_temps(start,end): 
    """ calculate the `TMIN`, `TAVG`, and `TMAX` for dates between the start and end date inclusive. """

    connection = engine.connect()
    def my_sql_calc_temps(start_date, end_date):

        sql_calc_temps_sql = "SELECT min(tobs), avg(tobs), max(tobs) from measurement where date BETWEEN '"+start_date+"'and '"+end_date+"' "
        result = connection.execute(sql_calc_temps_sql)

        all_temp_stats = []
        for row in result:
            all_temp_stats_dict = {}
            all_temp_stats_dict['min(tobs)'] = row['min(tobs)']
            all_temp_stats_dict['avg(tobs)'] = row['avg(tobs)']
            all_temp_stats_dict['max(tobs)'] = row['max(tobs)']
            all_temp_stats.append(all_temp_stats_dict)

            return(all_temp_stats)

    my_trip_temps = my_sql_calc_temps(start, end)
    connection.close()
    return jsonify(my_trip_temps)



@app.route("/api/v1.0/<start>")
def calc_one_date(start): 
    """ calculate the `TMIN`, `TAVG`, and `TMAX` for dates between the start and end date inclusive. """

    connection = engine.connect()
    def my_sql_calc_temps(start_date):

        sql_calc_temps_sql = "SELECT min(tobs), avg(tobs), max(tobs) from measurement where date > '"+start_date+"' "
        result = connection.execute(sql_calc_temps_sql)

        all_temp_stats = []
        for row in result:
            all_temp_stats_dict = {}
            all_temp_stats_dict['min(tobs)'] = row['min(tobs)']
            all_temp_stats_dict['avg(tobs)'] = row['avg(tobs)']
            all_temp_stats_dict['max(tobs)'] = row['max(tobs)']
            all_temp_stats.append(all_temp_stats_dict)

            return(all_temp_stats)

    my_trip_temps = my_sql_calc_temps(start)
    connection.close()
    return jsonify(my_trip_temps)


if __name__ == '__main__':
    app.run(debug=True)
